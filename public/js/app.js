
 var app = angular.module("app",[]	);

/**
 * angular factory provider to provide all http service
 * methods (get, put and post)
 * @param  url :url on which request to be send
 * @param param: param for get request
 * @param data for post and put request
 */
app.factory('httpService', function($http,$q){
	return {
		get : function(url,params){
			var deferred = $q.defer();
			$http({ method: "GET", 
					url: url,
					params:params,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					})
                .success(function (data, status, headers, config) {
                	
                    deferred.resolve({"data":data,"status":status});
                   
                }).error(function (data, status, headers, config) {
                	
                	if(status==401){
                		window.location=login_url;
                	}
                	if(status==500){
						toast("Ops some error occured");
					}
                    deferred.reject({"data":data,"status":status});
                    // console.log(data);	
                });
            return deferred.promise;
		},
		post : function(url,data){
			var deferred = $q.defer();
			$http({ method: "POST", 
					url: url,
					data:data,
					headers: { 'Content-Type': 'application/json' }
					})
                .success(function (data, status, headers, config) {
                    deferred.resolve({"data":data,"status":status});
                   
                    // console.log(data);
                }).error(function (data, status, headers, config) {
                	
                	if(status==401){
						window.location =login_url;
					}
                	if(status==500){
						toast("Ops some error occured");
					}
                    deferred.reject({"data":data,"status":status});
                    // console.log(data);	
                });
            return deferred.promise;
		},
		put : function(url,data){
			var deferred = $q.defer();
			$http({ method: "PUT", 
					url: url,
					data:data,
					headers: { 'Content-Type': 'application/json' }
					})
                .success(function (data, status, headers, config) {
                    deferred.resolve({"data":data,"status":status});
                    // console.log(data);
                }).error(function (data, status, headers, config) {
                	if(status==401){
						window.location =login_url;
					}
                	if(status==500){
						toast("Ops some error occured");
					}
                    deferred.reject({"data":data,"status":status});
                    // console.log(data);	
                });
            return deferred.promise;
		},
		delete : function(url,data){
			var deferred = $q.defer();
			$http({ method: "DELETE", 
					url: url,
					data:data,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					})
                .success(function (data, status, headers, config) {
                    deferred.resolve({"data":data,"status":status});
                    // console.log(data);
                }).error(function (data, status, headers, config) {
                	if(status==401){
						window.location =login_url;
					}
                	if(status==500){
						toast("Ops some error occured");
					}
                    deferred.reject({"data":data,"status":status});
                    console.log(data);	
                });
            return deferred.promise;
		}
	};
});
url = 'api/v1/todolist';
var scope;
app.controller('todoCtrl',['$scope','httpService', function($scope,httpService) {
    scope =$scope;
    $scope.todos = [];
    $scope.todo = {'title':"", 'desc':""};
    $scope.add = function() {
    	$scope.todo.reminder= Date.parse($scope.reminder)
    	$scope.todo.reminder = $scope.todo.reminder.toString().substring(0, 10);;
        httpService.post(url,$scope.todo)
		.then(function(result){
			if(result.status==201){
				toast("Item added");
				todo  = angular.copy($scope.todo);
				todo.id = result.data.data.id;
				$scope.todos.push(todo);
				$scope.todo = {'title':"", 'desc':""};
				$scope.reminder = null;
			}
		},function(result){

		});
    };
    httpService.get(url,{})
		.then(function(result){
			if(result.status==200){
				console.log(result.data );
				$scope.todos = result.data.data;
			}
		},function(result){

		});
 	var start_picker = new Pikaday({  
		field: document.getElementById('start_datepicker'),
		trigger: document.getElementById('start_datepicker-button'),
		minDate: new Date('2014-01-01'),
		maxDate: new Date('2020-12-31'),
		firstDay:1,
		yearRange: [2014,2020]
	});

	$scope.changeStatus = function(todo)
	{	
		httpService.put(url+"/"+todo.id, {})
		.then(function(result){
			if(result.status==200){
				toast("Status Changed");
			}
		},function(result){

		});
	}

	$scope.delete = function(todo)
	{
		httpService.delete(url+"/"+todo.id, {})
		.then(function(result){
			if(result.status==200){
				$scope.todos.splice($scope.todos.indexOf(todo),1);
				toast("Item deleted");
			}
		},function(result){

		});
	}
	$scope.toDate = function(date_time)
	{	
		try{
			var tim = date_time.split(/[- :]/);
			return new Date(tim[0], tim[1]-1, tim[2], tim[3], tim[4], tim[5]);
		}
		catch(err)
		{
			
		}
	}
}]);
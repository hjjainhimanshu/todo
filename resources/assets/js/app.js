
 var app = angular.module("app",['ngRoute']);
/**
 * angualr config to provide routing 
 */

// app.config(['$routeProvider',function($routeProvider){
// 	$routeProvider
// 	.when('/create',{
// 		templateUrl:template_url+'create',
// 		controller:'create_campaign_controller'
// 	})
// 	.otherwise({redirectTo:'/campaigns'});
// }]);
// 
/**
 * angular factory provider to provide all http service
 * methods (get, put and post)
 * @param  url :url on which request to be send
 * @param param: param for get request
 * @param data for post and put request
 */
campaign_manager.factory('httpService', function($http,$q){
	return {
		get : function(url,params){
			var deferred = $q.defer();
			$http({ method: "GET", 
					url: url,
					params:params,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					})
                .success(function (data, status, headers, config) {
                	
                    deferred.resolve({"data":data,"status":status});
                   
                }).error(function (data, status, headers, config) {
                	
                	if(status==401){
                		window.location=login_url;
                	}
                	if(status==500){
						toast("Ops some error occured");
					}
                    deferred.reject({"data":data,"status":status});
                    // console.log(data);	
                });
            return deferred.promise;
		},
		post : function(url,data){
			var deferred = $q.defer();
			$http({ method: "POST", 
					url: url,
					data:data,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					})
                .success(function (data, status, headers, config) {
                    deferred.resolve({"data":data,"status":status});
                   
                    // console.log(data);
                }).error(function (data, status, headers, config) {
                	
                	if(status==401){
						window.location =login_url;
					}
                	if(status==500){
						toast("Ops some error occured");
					}
                    deferred.reject({"data":data,"status":status});
                    // console.log(data);	
                });
            return deferred.promise;
		},
		put : function(url,data){
			var deferred = $q.defer();
			$http({ method: "PUT", 
					url: url,
					data:data,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					})
                .success(function (data, status, headers, config) {
                    deferred.resolve({"data":data,"status":status});
                    // console.log(data);
                }).error(function (data, status, headers, config) {
                	if(status==401){
						window.location =login_url;
					}
                	if(status==500){
						toast("Ops some error occured");
					}
                    deferred.reject({"data":data,"status":status});
                    // console.log(data);	
                });
            return deferred.promise;
		},
		delete : function(url,data){
			var deferred = $q.defer();
			$http({ method: "DELETE", 
					url: url,
					data:data,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					})
                .success(function (data, status, headers, config) {
                    deferred.resolve({"data":data,"status":status});
                    // console.log(data);
                }).error(function (data, status, headers, config) {
                	if(status==401){
						window.location =login_url;
					}
                	if(status==500){
						toast("Ops some error occured");
					}
                    deferred.reject({"data":data,"status":status});
                    console.log(data);	
                });
            return deferred.promise;
		}
	};
});
url = 'api/v1/todolist';
app.controller('todoCtrl', function($scope) {
    $scope.todos = [];
    $scope.add = function() {
        httpService.post(url,$scope.todo)
		.then(function(result){
			if(result.status==200){
				toast("Item added");
			
			}
		},function(result){

		});
    };
    httpService.get(url,{})
		.then(function(result){
			if(result.status==200){
				$scope.todos = result.data 
				toast("Item added");
			
			}
		},function(result){

		});
 
});
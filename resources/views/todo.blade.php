<html ng-app="app">
	<head>
		<title>TODO- List</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
		<link href='/css/pickaday.css' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.5/angular.js"></script>
		<style>
			body{
				background-color: #f2f2f2;
			}
			ul{list-style: none;}
			.reminder {margin-left: 10px;}
			.add_task{display: inline-block;}
			#start_datepicker{display: none;}
			#start_datepicker-button{ color: rgb(89,184,57);}
			/* toast */
			#toast-message { background: none repeat scroll 0 0 #fff; border-radius: 10px; bottom: 155px; height: 32px; left: 37%; margin: 0 auto; opacity: 1; position: fixed; text-align: center; transition: opacity 500ms ease 0s; width: 330px; z-index: 1000;}
			#toast-message h4 { color: #030; font-size: 15px; font-weight: 700; line-height: 28px; padding: 0 5px; margin: 0; }
			#toast-message.hide { display: none; opacity: 0; }
			/* toast end */
		</style>
	</head>
	<body ng-controller="todoCtrl">
		<div class="container">
			<div class="content">
				<div> 
					<div>
						<label class="add_task"> Add task </label><input ng-model="todo.task">
						<div class="date-picker add_task">
							 <aside> 
			                    <a tabindex="3" href="javascript:void(0);" id="start_datepicker-button"><i class="fa fa-calendar-o fa-lg"><span>Reminder </span></i></a>
			                    <input ng-model="reminder" type="text" id="start_datepicker">
			                </aside>
			                <span ng-if="reminder" class="start_date">@{{reminder}}</span>
			            </div>
			            <button class="add_task" ng-click="add()">Add</button>
					</div>
				</div>
				<h1>Todo List</h1>
				<ul>
					<li ng-repeat="item in todos" ng-show="todos">
						<span ng-show="item.status==1">
							<input type="checkbox" checked disabled>
						</span>
						<span><input ng-show="item.status!=1" type="checkbox" ng-model="item.status" ng-true-value="1" ng-false-value="0" ng-change="changeStatus(item)"></span><span>@{{item.task}}</span>&nbsp;<span class="reminder" ng-show="item.reminder">--- @{{toDate(item.reminder) | date:'medium'}}</span> <button ng-show="item.status!=1" ng-click="delete(item)">X</button>
					</li>
				</ul>
			</div>
		</div>
		<div id="toast-message" class="hide">
	        <h4 id="toastMessage">Hi</h4>
	    </div> 
	    <script>

        function toast(message){
            console.log("toast called "+message);
            // $scope.toastMessage = message;  
            document.getElementById('toastMessage').innerHTML = message;
            // if(typeof toastMessage=="undefined")
            //     var toast_message = document.getElementById('toast-message');
           
            document.getElementById('toast-message').className         = "";   
            setTimeout(function(){
                document.getElementById('toast-message').className     = 'hide';
            }, 2500);
        };
    </script>
    <script src="/js/app.js"></script>
    <script src="/js/pickaday.js"></script>
	</body>
</html>

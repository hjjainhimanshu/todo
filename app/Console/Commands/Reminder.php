<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use App\Todolist;
use Mail;


class Reminder extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'remind:task';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command to remind task';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$from_date = date('Y-m-d H:i:s');
		$to_date = new \Datetime();
        $to_date->modify('5 minutes'); 
        $todos = Todolist::where('reminder', '>', $from_date)->where('reminder', '<', $to_date->format('Y-m-d H:i:s'))->get()->toArray();
        print_R($todos);
        // exit
        foreach($todos as $item)
        {
        	print_r($item);
        	Mail::send('emails.reminder', array('task'=>$item['task']), function($message)
			{
			    $message->from('himanshujain.2792@gmail.com', 'Todos');

			    $message->to('himanshujain.2792@gmail.com');
			});
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}


}

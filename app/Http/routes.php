<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'TodoController@index');

// Route::get('home', 'HomeController@index');

Route::group(['prefix'=>'api/v1'],function(){
	Route::get('/todolist','TodoController@getList');
	Route::post('/todolist','TodoController@create');
	Route::put('/todolist/{id}','TodoController@update');
	Route::delete('/todolist/{id}','TodoController@delete');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

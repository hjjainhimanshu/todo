<?php namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use App\Todolist;
use Validator;

class TodoController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Todo Controller
	|--------------------------------------------------------------------------
	|
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('todo');
	}

	public function getList(Request $request)
	{
		$todos = DB::table('todo_list')->paginate(100);
		return Response::json($todos, 200);
	}

	public function create(Request $request)
	{
		$data = $request->only('task', 'reminder');
    
	    $validator = Validator::make($request->all(), [
            'task' => 'required|min:3',
        ]);
        if ($validator->fails()) {
            return Response::json(['data' => (object)array(), 'message' => $validator->messages(), 'error' => True], 400);
        }
        $todo = new Todolist;
        $todo->task = $data['task'];
        $todo->status = 0;
        $todo->reminder = isset($data['reminder']) ?date('Y-m-d H:i:s', ($data['reminder'])): null;
        $todo->save();
        return Response::json(['data' => array('id'=>$todo->id), 'message' => 'Item created', 'error' => False], 201);
	}

	public function update($id, Request $request)
	{
		$todo = Todolist::find($id);
		$todo->status = 1;
		$todo->completed_at = date('Y-m-d H:i:s');

		$todo->save();
		return Response::json(['data' => (object)array(), 'message' => 'Item update', 'error' => False], 200);
	}

	public function delete($id, Request $request)
	{
		$todo = Todolist::find($id);
		$todo->delete();
		return Response::json(['data' => (object)array(), 'message' => 'Item deleted', 'error' => False], 200);
	}
}

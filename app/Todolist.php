<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Todolist extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'todo_list';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['task', 'status', 'reminder'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['updated_at', 'created_at', 'deleted_at'];

	public function getReminderAttribute($value)
    {
    	// return strtotime($value);
    	$this->attributes['reminder'] = strtotime($value);
    }
 //    protected function getDateFormat($value)
	// {
	//     return strtotime($value);
	// }

}
